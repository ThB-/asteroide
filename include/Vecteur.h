#ifndef VECTEUR_H
#define VECTEUR_H




struct Vecteur
{
    void operator+=(Vecteur const& autre); // On explique au compilateur comment additioner deux vecteurs
    void operator+=(float const& x2);
    void operator-=(Vecteur const& autre);
    Vecteur operator*(float coeff)const ;
    static Vecteur creerDepuisAngle(float taille, float angleEnDegres);
    float x,y;

};

#endif // VECTEUR_H
