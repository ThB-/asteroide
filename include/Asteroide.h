#ifndef ASTEROIDE_H_INCLUDED
#define ASTEROIDE_H_INCLUDED
#include <SFML/Graphics.hpp>
#include "Vecteur.h"
#include "Coordonnees.h"


class Asteroide{

    public:
        Asteroide(sf::Color const& couleur);
        void mettreAJour(float temps, const float dx, const float dy);
        void afficher(sf::RenderWindow& fenetre) const;
        float getX();
        float getY();
        void EviterSortirEcranRandom();
        int getNum();


    private:
        sf::Texture texture;
        sf::Sprite sprite;
        Vecteur vitesse;
        Coordonnees position{};
        int x = 1;
        bool estSorti = false;
        Asteroide *listeAsteroide[5];


};





#endif // ASTEROIDE_H_INCLUDED
