#ifndef VAISSEAU_H
#define VAISSEAU_H
#include <SFML/Graphics.hpp>
#include "Vecteur.h"
#include "Coordonnees.h"
#include "Asteroide.h"




class Vaisseau{


    public:
        Vaisseau(sf::Color const& couleur); // Faire avancer le vaisseau
        void afficher(sf::RenderWindow& fenetre) const; // Afficher le vaisseau
        void actualiserEtat();
        void mettreAJour(float temps);
        bool testerCollisions(Asteroide & autre);

        float getX();
        float getY();



    private:
        sf::Texture texture;
        sf::Sprite sprite;
        bool accelerationEnCours{false};
        bool tourneAGauche{false};
        bool tourneADroite{false};
        Vecteur vitesse;
        Coordonnees position{};



        const float ACCELERATION{700.f};
        const float FROTTEMENTS{2.0f};
        const float VITESSEANGULAIRE{50.f};

};













#endif // VAISSEAU_H_INCLUDED
