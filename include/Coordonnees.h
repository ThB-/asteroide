#ifndef COORDONNEES_H
#define COORDONNEES_H

#include "Vecteur.h"

class Coordonnees{

    public:
        Coordonnees();
        float getX();
        float getY();
        static void initialisationFenetre(int largeur, int hauteur);
        void operator+=(Vecteur const& autre);
        void setX(const float valeur);
        void setY(const float valeur);

    private:

    Vecteur vecteur;



};




#endif // COORDONNEES_H
