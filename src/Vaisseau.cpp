#include <iostream>
#include <SFML/Graphics.hpp>
#include "Vaisseau.h"
#include <tgmath.h>
#include "Asteroide.h"


using namespace std;

Vaisseau::Vaisseau(sf::Color const& couleur){
    if (!texture.loadFromFile("ressources/vaisseau.png")){
        std::cerr << "L'image du vaisseau ne peut pas charger" << std::endl;
    }
    sprite.setTexture(texture);
    sprite.setColor(couleur);
    sprite.setOrigin(sprite.getLocalBounds().width / 2, sprite.getLocalBounds().height /2);
    sprite.setPosition(position.getX(),position.getY());
}



void Vaisseau::afficher(sf::RenderWindow& fenetre) const{
    fenetre.draw(sprite);

}

void Vaisseau::mettreAJour(float temps){
    if (accelerationEnCours){
        vitesse +=  Vecteur::creerDepuisAngle(ACCELERATION*temps,sprite.getRotation());
    }
    vitesse -= vitesse * FROTTEMENTS * temps;
    auto deplacement = vitesse*temps;
    position += deplacement;
    sprite.setPosition(fmod(position.getX(),800),fmod(position.getY(),600));



    if(tourneAGauche){
        sprite.rotate(- VITESSEANGULAIRE * temps);
    }

    if(tourneADroite){

        sprite.rotate(VITESSEANGULAIRE * temps);
    }
}

void Vaisseau::actualiserEtat(){

    accelerationEnCours = sf::Keyboard::isKeyPressed(sf::Keyboard::Z);
    tourneAGauche = sf::Keyboard::isKeyPressed(sf::Keyboard::Q);
    tourneADroite = sf::Keyboard::isKeyPressed(sf::Keyboard::D);


}


bool Vaisseau::testerCollisions(Asteroide & autre){
    if ((position.getX() - autre.getX()) > -50.f && position.getX() - autre.getX() < 50.f){
            return true;
        }
    else{
        auto var = (position.getX() - autre.getX());
        return false;
    }

}


float Vaisseau::getX(){

    return position.getX();

}

float Vaisseau::getY(){

    return position.getY();

}


