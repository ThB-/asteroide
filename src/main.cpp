#include <iostream>
#include <SFML/Graphics.hpp>
#include "Vaisseau.h"
#include "Coordonnees.h"
#include "Asteroide.h"
#include <stdlib.h>


using namespace std;
const int LARGEURFENETRE{800};
const int HAUTEURFENETRE{600};

int main()
{
    // Initialisation de l'�criture

    sf::Font font;
    font.loadFromFile("arial.ttf");


    // Cr�ation de la fen�tre

    // Affichage de la fen�tre
    sf::RenderWindow fenetre(sf::VideoMode(LARGEURFENETRE,HAUTEURFENETRE), "Asteroids !");
    Coordonnees::initialisationFenetre(LARGEURFENETRE,HAUTEURFENETRE);
    auto vaisseau = Vaisseau(sf::Color(255,255,255));
    auto asteroide = Asteroide(sf::Color(255,255,255));
    bool aCommence = false;
    auto chrono = sf::Clock{};
    while(fenetre.isOpen()){
        auto evenement = sf::Event();
        while(fenetre.pollEvent(evenement)){
            if(evenement.type == sf::Event::Closed){
                fenetre.close();
            }

        }


    float dx = rand ()% 50 + 0.50; // random
    float dy = rand ()% 75 + 0.50;
    auto time = chrono.restart().asSeconds();
    vaisseau.actualiserEtat();
    vaisseau.mettreAJour(time);
    asteroide.mettreAJour(time,dx,dy);
    fenetre.clear();
    vaisseau.afficher(fenetre); // afficher le vaisseau
    asteroide.afficher(fenetre); // afficher l'adteroide
    vaisseau.testerCollisions(asteroide); // detecte les collisions
    fenetre.display();
    }
    return 0;
}
