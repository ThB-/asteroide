#include <iostream>
#include <cmath>
#include "Vecteur.h"





void Vecteur::operator+=(Vecteur const& autre){
    x += autre.x;
    y += autre.y;

}

void Vecteur::operator-=(Vecteur const& autre){

    x-= autre.x;
    y-= autre.y;


}


Vecteur Vecteur::operator*(float coeff)const {
    return {x*coeff, y*coeff};

}


void Vecteur::operator+=(const float &x2){
     x+= x2;

     y+=x2;

}

Vecteur Vecteur::creerDepuisAngle(float taille, float angleEnDegres){

    return {taille*cos(angleEnDegres/180.f*3.14), taille*sin(angleEnDegres/180.f*3.14)};

}

