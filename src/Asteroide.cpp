#include <iostream>
#include <stdlib.h>
#include "Asteroide.h"
#include <tgmath.h>
#include <SFML/Graphics.hpp>

using namespace std;


Asteroide::Asteroide(sf::Color const& couleur){
    if (!texture.loadFromFile("ressources/asteroide.png")){
        std::cerr << "L'image de l'ast�roide ne peut pas charger" << std::endl;
    }
    sprite.setTexture(texture);
    sprite.setColor(couleur);
    sprite.setOrigin(sprite.getLocalBounds().width / 2, sprite.getLocalBounds().height /2);
    vitesse = {200.f,75.f};
    x +=1;

}



void Asteroide::afficher(sf::RenderWindow& fenetre) const{
        fenetre.draw(sprite);

}

void Asteroide::mettreAJour(float temps,const float dx, const float dy){
    auto deplacement = vitesse*temps;
    position += deplacement;
    sprite.setPosition(fmod(getX()+dx*temps,800),fmod(getY()+dy*temps,600));

}





float Asteroide::getX(){

    return position.getX();

}


float Asteroide::getY(){

    return position.getY();

}


int Asteroide::getNum(){

    return x;
}


